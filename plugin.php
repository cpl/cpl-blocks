<?php
/**
 * Plugin Name: cpl-blocks
 * Plugin URI: https://gitlab.com/cpl/cpl-blocks
 * Description: Customized blocks and styles for CPL.org
 * Author: Will Skora and the CPL team
 * Author URI: https://gitlab.com/cpl/cpl-blocks
 * Version: 1.0.3
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
