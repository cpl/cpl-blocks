//  Import CSS.
import './editor.scss';
import './style.scss';


const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks

wp.blocks.registerBlockStyle( 'core/button', {
    name: 'cpl-button--action',
    label: 'CPL\'s action',
} );